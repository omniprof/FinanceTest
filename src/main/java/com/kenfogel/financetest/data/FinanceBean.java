package com.kenfogel.financetest.data;

import java.math.BigDecimal;
import java.util.Objects;

public class FinanceBean {

    private BigDecimal payment;
    private BigDecimal presentValue;
    private BigDecimal rate;
    private BigDecimal term;
    private BigDecimal futureValue;

    public FinanceBean() {
        payment = new BigDecimal("0");
        presentValue = new BigDecimal("0");
        rate = new BigDecimal("0");
        term = new BigDecimal("0");
        futureValue = new BigDecimal("0");
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getPresentValue() {
        return presentValue;
    }

    public void setPresentValue(BigDecimal presentValue) {
        this.presentValue = presentValue;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getTerm() {
        return term;
    }

    public void setTerm(BigDecimal term) {
        this.term = term;
    }

    public BigDecimal getFutureValue() {
        return futureValue;
    }

    public void setFutureValue(BigDecimal futureValue) {
        this.futureValue = futureValue;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.payment);
        hash = 43 * hash + Objects.hashCode(this.presentValue);
        hash = 43 * hash + Objects.hashCode(this.rate);
        hash = 43 * hash + Objects.hashCode(this.term);
        hash = 43 * hash + Objects.hashCode(this.futureValue);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FinanceBean other = (FinanceBean) obj;
        if (!Objects.equals(this.payment, other.payment)) {
            return false;
        }
        if (!Objects.equals(this.presentValue, other.presentValue)) {
            return false;
        }
        if (!Objects.equals(this.rate, other.rate)) {
            return false;
        }
        if (!Objects.equals(this.term, other.term)) {
            return false;
        }
        if (!Objects.equals(this.futureValue, other.futureValue)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FinanceBean [payment=" + payment + ", presentValue="
                + presentValue + ", rate=" + rate + ", term=" + term
                + ", futureValue=" + futureValue + "]";
    }
}
