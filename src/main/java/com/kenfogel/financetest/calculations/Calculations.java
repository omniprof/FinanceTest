package com.kenfogel.financetest.calculations;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import com.kenfogel.financetest.data.FinanceBean;

public class Calculations {

    public Calculations() {
    }

    public void loanCalculation(FinanceBean fb) throws ArithmeticException {

        BigDecimal temp;
        // (1+rate)
        temp = BigDecimal.ONE.add(fb.getRate());
        // (1+rate)^term
        temp = temp.pow(fb.getTerm().intValueExact());
        // BigDecimal pow does not support negative exponents so divide 1 by the result
        temp = BigDecimal.ONE.divide(temp, MathContext.DECIMAL64);
        // 1 - (1+rate)^-term
        temp = BigDecimal.ONE.subtract(temp);
        // rate / (1 - (1+rate)^-term)
        temp = fb.getRate().divide(temp, MathContext.DECIMAL64);
        // pv * (rate / 1 - (1+rate)^-term)
        temp = fb.getPresentValue().multiply(temp);
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);
        fb.setPayment(temp.abs());
    }

    public void futureValueCalculation(FinanceBean fb) throws ArithmeticException {
        BigDecimal temp;
        // (1+rate)
        temp = BigDecimal.ONE.add(fb.getRate());
        // (1+rate)^term
        temp = temp.pow(fb.getTerm().intValueExact());
        // 1 - (1+rate)^term
        temp = BigDecimal.ONE.subtract(temp);
        // (1 - (1+rate)^-term) / rate
        temp = temp.divide(fb.getRate(),MathContext.DECIMAL64);
        // pmt * ((1 - (1+rate)^-term) / rate)
        temp = fb.getPayment().multiply(temp);
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);
        fb.setFutureValue(temp.abs());

    }

    public void savingsGoalCalculation(FinanceBean fb) throws ArithmeticException {
        BigDecimal temp;
        // (1+rate)
        temp = BigDecimal.ONE.add(fb.getRate());
        // (1+rate)^term
        temp = temp.pow(fb.getTerm().intValueExact());
        // 1 - ((1+rate)^term)
        temp = BigDecimal.ONE.subtract(temp);
        // rate / (1 - (1+rate)^term)
        temp = fb.getRate().divide(temp, MathContext.DECIMAL64);
        // fv * (rate / (1 - (1+rate)^term))
        temp = fb.getFutureValue().multiply(temp);
        temp = temp.setScale(2, RoundingMode.HALF_EVEN);
        fb.setPayment(temp.abs());
    }
}
