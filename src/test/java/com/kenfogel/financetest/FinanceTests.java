package com.kenfogel.financetest;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.kenfogel.financetest.calculations.Calculations;
import com.kenfogel.financetest.data.FinanceBean;

public class FinanceTests {

    /*
     * Loan PV 5000 rate 0.004166667 term 60 PMT $94.36
     * 
     * Future PMT 100 rate 0.004166667 term 60 FV $6,800.61
     * 
     * Savings FV 6800.61 rate 0.004166667 term 60 PMT $100.00
     */
    private Calculations calc;
    private FinanceBean fb;

    @Before
    public void init() {
        calc = new Calculations();
        fb = new FinanceBean();
    }

    @Test
    public void knowValueLoanCalculationTest() {
        fb.setPresentValue(new BigDecimal("5000"));
        fb.setRate(new BigDecimal("0.004166667"));
        fb.setTerm(new BigDecimal("60"));
        calc.loanCalculation(fb);
        System.out.println(fb.toString());
        assertEquals(new BigDecimal("94.36"), fb.getPayment());
    }

    @Test
    public void knowValueFutureCalculationTest() {
        fb.setPayment(new BigDecimal("100"));
        fb.setRate(new BigDecimal("0.004166667"));
        fb.setTerm(new BigDecimal("60"));
        calc.futureValueCalculation(fb);
        System.out.println(fb.toString());
        assertEquals(new BigDecimal("6800.61"), fb.getFutureValue());
    }

    @Test
    public void knowValueSavingsCalculationTest() {
        fb.setFutureValue(new BigDecimal("6800.61"));
        fb.setRate(new BigDecimal("0.004166667"));
        fb.setTerm(new BigDecimal("60"));
        calc.savingsGoalCalculation(fb);
        System.out.println(fb.toString());
        assertEquals(new BigDecimal("100.00"), fb.getPayment());
    }

}
